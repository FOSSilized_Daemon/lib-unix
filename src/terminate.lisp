(in-package :os)

;;;; The terminate.lisp file implements variables, functions,
;;;; and other functionality for terminating the current Lisp process
;;;; on operating systems in ANSI compliant Common Lisp. The entirety of
;;;; the below codebase is written to be easily understood and
;;;; reprogrammable from any ANSI compliant Common Lisp implementation.

(defun quit (&optional (status-code 0)
                       (finish-buffered-output t))
  "Quits the current Common Lisp process."
  (when finish-buffered-output
    (finish-outputs))

  (or #+allegro      (excl:exit status-code :quit t)
      #+abcl         (ext:quit :status status-code)
      #+clasp        (si:quit status-code)
      #+clisp        (ext:quit status-code)
      #+clozure      (ccl:quit status-code)
      #+cmucl        (unix:unix-exit status-code)
      #+cormanlisp   (win32:exitprocess status-code)
      #+ecl          (ext:quit status-code)
      #+gcl          (system:quit status-code)
      #+lispworks    (libsworks:quit :status status-code :confirm nil :return nil :ignore-errors-p t)
      #+mcl          (progn status-code (ccl:quit))
      #+mkcl         (mk-ext:quit :exit-code status-code)
      #+openmcl      (progn status-code (ccl:quit))
      #+poplog       (progn status-code (poplog:bye))
      #+sbcl         (sb-ext:quit :unix-status status-code :recklessly-p (not finish-buffered-output))
      #+scl          (unix:unix-exit status-code)
      #+ufasoft-lisp (progn status-code (ext:quit))
      #+wcl          (progn status-code (lisp:quit))
      #+xcl          (ext:quit :status status-code)
      (sll:log-unsupported-implementation "cl-os" "(QUIT)")))

(setf (fdefinition 'exit) #'quit)
