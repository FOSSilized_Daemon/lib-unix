(in-package :os)

;;;; The kernel.lisp file implements variables, functions, and other
;;;; functionality for working with kernel information on operating
;;;; systems in ANSI compliant Common Lisp. The entirety of the below
;;;; codebase is written to be easily understood and reprogrammable
;;;; from any ANSI compliant Common Lisp implementation.

(defun os-linux-p ()
  "Checks to see if the current kernel is Linux."
  (or (string= "Linux" (software-type))
      (featurep :linux)))

(defun os-openbsd-p ()
  "Checks to see if the current kernel is OpenBSD."
  (or (string= "OpenBSD" (software-type))
      (featurep :openbsd)))

(defun os-freebsd-p ()
  "Checks to see if the current kernel is FreeBSD."
  (or (string= "FreeBSD" (software-type))
      (featurep :freebsd)))

(defun os-netbsd-p ()
  "Checks to see if the current kernel is NetBSD."
  (or (string= "NetBSD" (software-type))
      (featurep :netbsd)))

(defun os-macosx-p ()
  "Checks to see if the current kernel is MacOSX."
  (or (string= "MacOSX" (software-type))
      (string= "Darwin" (software-type))
      (featurep '(:or :darwin (:and :allegro :macosx) (:and :clisp :macos)))))

(defun os-oldmac-p ()
  "Checks to see if the current kernel is one of the pre-MacOSX kernels."
  ;; Unclear what should be checked for as this covers many OSes in UIOP.
  (featurep :mcl))

(defun os-plan9-p ()
  "Checks to see if the current kernel is Plan9."
  (or (string= "Plan9" (software-type))
      (featurep :plan9)))

(defun os-haiku-p ()
  "Checks to see if the current kernel is Haiku."
  (or (string= "Haiku" (software-type))
      (featurep :haiku)))

(defun os-unix-p ()
  "Checks to see if the current kernel is a UNIX or UNIX-like kernel."
  (or (os-linux-p)
      (os-openbsd-p)
      (os-freebsd-p)
      (os-netbsd-p)
      (os-macosx-p)
      (os-plan9-p)
      (os-haiku-p)
      (featurep '(:or :unix :cygwin))))

(defun os-windows-p ()
  "Checks to see if the current kernel is a Windows kernel."
  ;; Unclear what should be checked for as I do not have a Windows system
  ;; to test for the value of (software-type).
  (and (not (os-unix-p)) (featurep '(:or :win32 :windows :mswindows :mingw32 :mingw64))))

(defun os-genera-p ()
  "Checks to see if the current kernel is a Genera kernel."
  (featurep :genera))

(defun os-mezzano-p ()
  "Checks to see if the current kernel is a Mezzano kernel."
  (featurep :mezzano))

(defun detect-os ()
  "Checks to see what the current kernel is."
  (loop for feature-name in '(:linux  :openbsd :freebsd :netbsd
                              :macosx :oldmac  :plan9   :haiku
                              :unix   :windows :genera  :mezzano)
        when (featurep feature-name)
        return feature-name))

(setf (fdefinition 'operating-system) #'detect-os)
