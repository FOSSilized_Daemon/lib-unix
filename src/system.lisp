(in-package :os)

;;;; The system.lisp file implements variables, functions,
;;;; and other functionality for working with system information
;;;; on operating systems in ANSI compliant Common Lisp.
;;;; The entirety of the below codebase is written to be easily
;;;; understood and reprogrammable from any ANSI compliant
;;;; Common Lisp implementation.

(defun hostname ()
  "Returns the hostname of the current operating system."
  (or #+(or abcl clasp clozure cmucl ecl genera lispworks mcl mezzano mkcl sbcl scl xcl) (machine-instance)
      #+cormanlisp "localhost"
      #+allegro (excl.osi:gethostname)
      #+clisp (first (sml:split-string (machine-instance) #\Space))
      #+gcl (system:gethostname)
      (sll:log-unsupported-implementation "cl-os" "(hostname)")))
