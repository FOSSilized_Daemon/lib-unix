(in-package :os)

;;;; The file-system.lisp file implements variables, functions, and other
;;;; functionality for working with the file system on operating
;;;; systems in ANSI compliant Common Lisp. The entirety of the below
;;;; codebase is written to be easily understood and reprogrammable
;;;; from any ANSI compliant Common Lisp implementation.

(defun getcwd ()
  "Get the current working directory."
    (or #+(or abcl genera mezzano xcl) (truename *default-pathname-defaults*)
        #+(or clasp ecl)               (ext:getcwd)
        #+allegro                      (excl::current-directory)
        #+clisp                        (ext:default-directory)
        #+clozure                      (ccl:current-directory)
        #+gcl                          (let ((*default-pathname-defaults* #p"")) (truename #p""))
        #+lispworks                    (hcl:get-working-directory)
        #+mkcl                         (mk-ext:getcwd)
        #+sbcl                         (sb-ext:parse-native-namestring (sb-unix:posix-getcwd/))
        #+xcl                          (extensions:current-directory)
        (sll:log-unsupported-implementation "cl-os" "(getcwd)")))

(defun chdir (target-directory)
  "Change the current working directory to TARGET-DIRECTORY."
  (let ((directory (pathname target-directory)))
        (or
          #+(or abcl genera mezzano xcl) (setf *default-pathname-defaults* (truename directory))
          #+(or cmucl scl)               (unix:unix-chdir (ext:unix-namestring directory))
          #+allegro                      (excl:chdir directory)
          #+clisp                        (ext:cd directory)
          #+clozure                      (setf (ccl:current-directory) directory)
          #+ecl                          (ext:chdir directory)
          #+clasp                        (ext:chdir direcory t)
          #+gcl                          (system:chdir directory)
          #+lispworks                    (hcl:change-directory directory)
          #+mkcl                         (mk-ext:chdir directory)
          #+sbcl                         (progn (require 'sb-posix) (sb-posix:chdir (sb-ext:native-namestring directory)))
          (sll:log-unsupported-implementation "cl-os" "(chdir)"))))

(setf (fdefinition 'cwd) #'getcwd
      (fdefinition 'pwd) #'getcwd
      (fdefinition 'cd)  #'chdir)
