(in-package :os)

;;;; The stream.lisp file implements variables, functions, and other
;;;; functionality for working with streams on operating systems in
;;;; ANSI compliant Common Lisp. The entirety of the below codebase
;;;; is written to be easily understood and reprogrammable from any
;;;; ANSI compliant Common Lisp implementation.

;; Libraries like UIOP simply store the initial value of the standard streams
;; in a more UNIX friendly name. While this has its uses it also has an annoying
;; limitation of the UNIX streams, "stdin" for example, not changing when their
;; respective streams do. In order to be more UNIX friendly and dynamic, this
;; library will simply create the UNIX streams as synonyms to their Common Lisp variants.
(defvar *stdin*
  (or #+clasp   (make-synonym-stream 'ext::+process-standard-input+)
      #+clozure (make-synonym-stream 'ccl:*stdin*)
      #+cmucl   (make-synonym-stream 'system:*stdin*)
      #+ecl     (make-synonym-stream 'ext::+process-standard-input+)
      #+sbcl    (make-synonym-stream 'sb-sys:*stdin*)
      #+scl     (make-synonym-stream 'system:*stdin*)
      #-(or clasp clozure cmucl ecl sbcl scl) (sll:log-unsupported-implementation "cl-os" "*STDIN*"))
  "A synonymous stream of standard-input.")

(defvar *stdout*
  (or #+clasp   (make-synonym-stream 'ext::+process-standard-output+)
      #+clozure (make-synonym-stream 'ccl:*stdout*)
      #+cmucl   (make-synonym-stream 'system:*stdout*)
      #+ecl     (make-synonym-stream 'ext::+process-standard-output+)
      #+sbcl    (make-synonym-stream 'sb-sys:*stdout*)
      #+scl     (make-synonym-stream 'system:*stdout*)
      #-(or clasp clozure cmucl ecl sbcl scl) (sll:log-unsupported-implementation "cl-os" "*STDOUT*"))
  "A synonymous stream of standard-output.")

(defvar *stderr*
  (or #+clasp   (make-synonym-stream 'ext::+process-error-output+)
      #+clozure (make-synonym-stream 'ccl:*stderr*)
      #+cmucl   (make-synonym-stream 'system:*stderr*)
      #+ecl     (make-synonym-stream 'ext::+process-error-output+)
      #+sbcl    (make-synonym-stream 'sb-sys:*stderr*)
      #+scl     (make-synonym-stream 'system:*stderr*)
      #-(or clasp clozure cmucl ecl sbcl scl) (sll:log-unsupported-implementation "cl-os" "*STDERR*"))
  "A synonymous stream of standard-error.")

(defun finish-outputs (&rest output-streams)
  "Ensures any buffered output has reached its destination."
  (setf output-streams (append output-streams
                              (list *stdout* *stderr* *standard-output* *error-output* *trace-output*
                                    *debug-io* *terminal-io* *query-io*)))

  (loop for target-stream in output-streams
    do (ignore-errors (finish-output target-stream))))

; (defun copy-stream-to-stream (input-stream output-stream &key element-type buffer-size line-by-line line-prefix)
;   "Copies the contents of INPUT-STREAM to OUTPUT-STREAM verbatim."
;   (with-open-stream (input-stream input-stream)
;     (if line-by-line
;       (loop :for (current-line eof) = (multiple-value-list (read-line input-stream nil nil))
;             :while current-line do
;             ;; As we do not want to change anything about
;             ;; the contents of input-stream use (pint1).
;             (when line-prefix
;               (prin1 prefix output-stream))
;             (prin1 current-line output-stream)

;             (unless eof
;               (terpri output-stream))
;             (finish-output output-stream)

;             (when eof
;               (return 0)))
;       ;; TODO: Implement (write-sequence ())
;       )))
