(defpackage :os
  (:use :common-lisp :asdf)
  (:export
    ;; environment-variable.lisp
    #:getenv
    #:getenvp
    #:getenv-pathname
    #:setenv
    #:setenv-if-not-p
    #:list-environment-variables

    ;; implementation.lisp
    #:featurep
    #:first-feature
    #:implementation-type
    #:detect-architecture
    #:lisp-version-string
    #:implementation-identifier

    ;; kernel.lisp
    #:os-linux-p
    #:os-openbsd-p
    #:os-freebsd-p
    #:os-netbsd-p
    #:os-macosx-p
    #:os-oldmac-p
    #:os-plan9-p
    #:os-haiku-p
    #:os-unix-p
    #:os-windows-p
    #:os-genera-p
    #:os-mezzano-p
    #:detect-os
    #:operating-system

    ;; stream.lisp
    #:*stdin*
    #:*stdout*
    #:*stderr*
    #:finish-outputs

    ;; system.lisp
    #:hostname

    ;; terminate.lisp
    #:quit
    #:exit

    ;; file-system.lisp
    #:getcwd
    #:cwd
    #:pwd
    #:chdir
    #:cd))
