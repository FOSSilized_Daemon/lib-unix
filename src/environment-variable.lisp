(in-package :os)

;;;; The environment-variable.lisp file implements variables,
;;;; functions, and other functionality for working with
;;;; environment variables on operating systems in ANSI
;;;; compliant Common Lisp. The entirety of the below
;;;; codebase is written to be easily understood and
;;;; reprogrammable from any ANSI compliant Common Lisp
;;;; implementation.

(defun getenv (environment-variable)
  "Get the value of ENVIRONMENT-VARIABLE."
  (declare (ignorable environment-variable))

  (or #+(or abcl clasp clisp ecl xcl) (ext:getenv environment-variable)
      #+allegro                       (sys:getenv environment-variable)
      #+clozure                       (ccl:getenv environment-variable)
      #+cmucl                         (unix:unix-getenv environment-variable)
      #+scl                           (cdr (assoc environment-variable ext:*environment-list* :test #'string=))
      #+gcl                           (system:getenv environment-variable)
      #+lispworks                     (lispworks:environment-variable environment-variable)
      #+sbcl                          (sb-ext:posix-getenv environment-variable)
      #-(or abcl clasp clisp ecl xcl allegro clozure cmucl scl gcl lispworks sbcl) (sll:log-unsupported-implementation "cl-os" "(getenv)")))

(defun getenvp (environment-variable)
  "Returns the value of ENVIRONMENT-VARIABLE if it is not nil."
  (let ((environment-value (getenv environment-variable)))
        (when (not (sml:empty-p environment-value))
          environment-value)))

(defun getenv-pathname (environment-variable)
  "Returns the value of ENVIRONMENT-VARIABLE as a pathname."
  (pathname (getenv environment-variable)))

(defsetf getenv (environment-variable) (variable-value)
  "Sets the value of ENVIRONMENT-VARIABLE to VARIABLE-VALUE."
  (declare (ignorable environment-variable variable-value))

  (or #+allegro        `(setf (sys:getenv ,environment-variable) ,variable-value)
      #+clisp          `(system::setenv ,environment-variable ,variable-value)
      #+clozure        `(ccl:setenv ,environment-variable ,variable-value)
      #+cmucl          `(unix:unix-setenv ,environment-variable ,variable-value 1)
      #+(or ecl clasp) `(ext:setenv ,environment-variable ,variable-value)
      #+lispworks      `(setf (lispworks:environment-variable ,environment-variable) ,variable-value)
      #+mkcl           `(mkcl:setenv ,environment-variable ,variable-value)
      #+sbcl           `(progn (require :sb-posix) (sb-posix:setenv ,environment-variable ,variable-value 1))
      #-(or clasp clisp ecl allegro clozure cmucl lispworks mkcl sbcl) (sll:log-unsupported-implementation "cl-os" "(getenv)")))

(defun setenv (environment-variable variable-value)
  "Sets the value of ENVIRONMENT-VARIABLE to VARIABLE-VALUE."
  (setf (getenv environment-variable) variable-value))

(defun setenv-if-not-p (environment-variable environment-value)
  "Set the value of ENVIRONMENT-VARIABLE to ENVIRONMENT-VALUE if it is not already set."
  (when (not (getenv environment-variable))
    (setf (getenv environment-variable) environment-value)))

(defun list-environment-variables ()
  "List all of the currently set environment variables."
  (or #+sbcl (sb-ext:posix-environ)
      #-sbcl (sll:log-unsupported-implementation "cl-os" "(list-environment-variables)")))
