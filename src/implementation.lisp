(in-package :os)

;;;; The implementation.lisp file implements variables, functions,
;;;; and other functionality for working with Common Lisp implementation
;;;; information on operating systems in ANSI compliant Common Lisp.
;;;; The entirety of the below codebase is written to be easily
;;;; understood and reprogrammable from any ANSI compliant
;;;; Common Lisp implementation.

(defun featurep (feature-expression &optional (*features* *features*))
  "Checks for whether or not FEATURE-EXPRESSION exists in *features*."
  (cond
    ((atom feature-expression)
     (and (member feature-expression *features*) t))
    ((eq :not (car feature-expression))
     (assert (null (cddr feature-expression)))
     (not (featurep (cadr feature-expression))))
    ((eq :or (car feature-expression))
     (some #'featurep (cdr feature-expression)))
    ((eq :and (car feature-expression))
     (every #'featurep (cdr feature-expression)))))

(defun first-feature (feature-list)
  "Returns the name of the first found feature in FEATURE-LIST within *features*."
  (loop for current-feature in feature-list
    when (featurep current-feature)
    return current-feature))

(defun implementation-type ()
  "Returns the current Common Lisp implementation in shorthand."
  (first-feature
    '(:abcl :acl :allegro :ccl :clozure :clisp :corman :cormanlisp
      :cmu :cmucl :cmu :clasp :ecl :gcl :lwpe :lispworks-personal-edition
      :lw :lispworks :mcl :mezzano :mkcl :sbcl :scl :smbx :symbolics :xcl)))

(defun detect-architecture ()
  "Returns the current Common Lisp architecture in shorthand."
  (first-feature
    '(:x64 :x86-64 :x86_64 :x8664-target :amd64 :word-size=64 :pc386
      :x86 :x86 :i386 :i486 :i586 :i686 :pentium3 :pentium4 :pc386
      :iapx386 :x8632-target :ppc64 :ppc64 :ppc64-target :ppc32 :ppc32
      :ppc32-target :ppc :powerpc :hppa64 :hppa :sparc64 :sparc32
      :sparc32 :sparc :mipsel :mipseb :mips :alpha :arm64 :arm64
      :aarch64 :armv8l :armv8b :aarch64_be :|aarch64| :arm :arm
      :arm-target :vlm :imach :java :java :java-1.4 :java-1.5
      :java-1.6 :java-1.7)))

(defun lisp-version-string ()
  "Returns a string that identifies the current Common Lisp implementation version."
  (let ((version-string (lisp-implementation-version)))
    version-string))

(defun implementation-identifier ()
  "Returns a string of the current Common Lisp implementation, system architecture, and kernel name."
  (format nil "~(~a~@{~@[-~a~]~}~)"
    (implementation-type)
    (lisp-version-string)
    (detect-os)
    (detect-architecture)))

(defvar *implementation-type* (implementation-type)
  "The current Common Lisp implementation in shorthand.")
