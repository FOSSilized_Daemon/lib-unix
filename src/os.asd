(defsystem "os"
  :name "os"
  :version "0.0.1"
  :maintainer "FOSSilized_Daemon (fossilizeddaemon@protonmail.com)"
  :author "FOSSilized_Daemon (fossilizeddaemon@protonmail.com)"
  :license "AGPL (see LISENCE for details)."
  :description "A common-lisp library for working with operating systems."
  :long-description "A common-lisp library for working with operating systems."
  :depends-on ("sml" "sll")
  :components ((:file "package")
               (:file "environment-variable")
               (:file "implementation")
               (:file "kernel")
               (:file "stream")
               (:file "system")
               (:file "terminate")
               (:file "file-system")))
